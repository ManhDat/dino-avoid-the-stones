﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameGUIManager : Singleton<GameGUIManager>
{
    public GameObject homeGUI;
    public GameObject gameGUI;

    public Dialog gameDiaLog;
    public Dialog pauseDiaLog;
    
    public Image fireRateFilled;
    public Text timerText;
    public Text killedCoutingText;

    public Dialog m_curDialog;

    public Dialog CurDialog { get => m_curDialog; set => m_curDialog = value; }

    public override void Awake()
    {
        MakeSingleton(false);
    }
    
    public void ShowGameGUI(bool isShow)
    {
        if (gameGUI)
            gameGUI.SetActive(isShow);
        if (homeGUI)
            homeGUI.SetActive(!isShow);            
    }
    public void UpdateTimer(string time)
    {
        if (timerText)
            timerText.text = time;
    }
    public void UpDateKilledCouting(int killed)
    {
        if (killedCoutingText)
            killedCoutingText.text = "x" + killed.ToString();
    }
    public void UpDateFireRate(float rate)
    {
        if (fireRateFilled)
            fireRateFilled.fillAmount = rate;    
    }
    public void PauseGame()
    {
        Time.timeScale = 0;
        if (pauseDiaLog)
        {
            pauseDiaLog.ShowDialog(true);
            pauseDiaLog.UpdateDiaLog("GAME PAUSE", "BEST KILLED : x" + Prefs.bestScore);
            m_curDialog = pauseDiaLog;  
        }
           
    }
    public void ResumeGame()
    {
        Time.timeScale = 1;
        if (m_curDialog)
            m_curDialog.ShowDialog(false);
    }
    public void BackToHome()
    {
        ResumeGame();
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
    public void RePlay()
    {
        if (m_curDialog)
            m_curDialog.ShowDialog(false);
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        
    }
    public void ExitGame()
    {
        ResumeGame();
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);

        Application.Quit();     

    }
}
